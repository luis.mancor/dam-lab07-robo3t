const express = require("express");
const mongoose = require("mongoose");
const app = express();
const bodyParser = require("body-parser");
const rutas = require("./routes/index");
const port = process.env.PORT || 3000 //MODIFICACION
//*************************************** */
//const bcrypt = require("bcrypt");  //npm install --save bcrypt  INSTALAR DEPENDENCIA EN CONSOLA FALTA
//*************************************** */
//const Usuario = require("./models/usuario");     //MOVIDO
//var usuario = require("./routes/usuario");

//crea urlencoded app
app.use(bodyParser.urlencoded({urlencoded: false}));
//crea app json parser
app.use(bodyParser.json());

//app.use(express.static(__dirname + '/routes')); //OWN
app.use(rutas);

mongoose.connect('mongodb://localhost:27017/lab07DB', (err, res) =>{
    if(err) throw err;
    console.log('Base de datos Online')
});



//app.listen(process.env.PORT, () => console.log(`Escuchando puerto ${process.env.PORT}`));

app.listen(port, ()=> console.log(`escuchando en puerto ${port}` ));