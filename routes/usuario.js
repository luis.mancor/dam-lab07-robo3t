const express = require("express");
const bcrypt = require("bcrypt");  //npm install --save bcrypt

const Usuario = require("../models/usuario");
const app = express();
const router = express.Router();

const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const port = process.env.PORT || 3000 //MODIFICACION
//*************************************** */
//const bcrypt = require("bcrypt");  //npm install --save bcrypt  INSTALAR DEPENDENCIA EN CONSOLA FALTA


app.post("/usuario", function (req, res){  //FUNCION MOVIDA
    let body = req.body;

    let usuario = new Usuario({
        nombre: body.nombre,
        email: body.email,
        password: bcrypt.hashSync(body.password, 10),
        role: body.role,
    });

    usuario.save((err, usuarioDB) => {
        if (err){
            return res.status(400).json({
                ok: false,
                err,
            });
        }
        usuarioDB.password = null;

        res.json({
            ok: true,
            usuario: usuarioDB,
        });
    });

});

app.get("/usuario", function (req, res){
    Usuario.find({}).exec((err, usuarios) => {
        if (err){
            return res.status(400).json({
                ok: false,
                err,
            });
        }
        res.json({
            ok: true,
            usuarios,
        });

    });
});



app.put("/usuario/:id", function (req, res) {
    let id = req.params.id;
    let body = req.body;

    Usuario.findByIdAndUpdate(id, body, {new: true}, (err, usuarioDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err,
            });
        }
        res.status(200).json({
            ok: true,
            usuario: usuarioDB,
        });

    });
});


app.delete("/usuario/:id", function(req, res){

    let id = req.params.id;
    Usuario.findByIdAndDelete(id, (err, usuarioBorrado)=>{
        if (err){
            return res.status(400).json({
                ok:false,
                err,
            });
        }
        res.json({
            ok:true,
            usuario: usuarioBorrado,
        });
    });
});

module.exports = app;