const express = require("express");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const Usuario = require("../models/usuario");
const app = express();

app.post("/login", (req, res) => {

    let body = req.body;

    Usuario.findOne({email: body.email}, (err, usuarioDB) => {
        //Control de error
        if (err){
            return res.status(500).json({
                ok:false,
                err,
            });
        }
        //Control de usuario en blanco
        if(!usuarioDB){
            return res.status(400).json({
                ok:false,
                err:{
                    message: "(Usuario) o contraseña incorrectas",
                },
            });
        }
        //Control de que las contraseñas no son iguales
        if(!bcrypt.compareSync(body.password, usuarioDB.password)){
            return res.status(400).json({
                ok: false,
                err:{
                    message: "Usuario o (contraseña) incorrectos",
                },
            });
        }

        res.json({
            ok: true,
            usuario: usuarioDB,
            token: '123',
        })

    });



});

module.exports = app;